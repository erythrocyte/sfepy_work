r"""
Laplace equation (e.g. temperature distribution) on a cube geometry with
different boundary condition values on the cube sides. This example was
used to create the SfePy logo.

Find :math:`T` such that:

.. math::
    \int_{\Omega} c \nabla s \cdot \nabla T
    = 0
    \;, \quad \forall s \;.
"""
from __future__ import absolute_import
import os

my_dir = os.path.dirname(os.path.realpath(__file__))
del_str = '/scripts'
if my_dir.endswith(del_str):
    my_dir = my_dir[:-len(del_str)]
print(my_dir)

filename_mesh = my_dir + '/meshes/3d/cube_tetrahedron.vtk'

# Laplace.

material_1 = {
    'name': 'coef',
    'values': {'val': 1.0},
}

field_1 = {
    'name': 'temperature',
    'dtype': 'real',
    'shape': (1,),
    'region': 'Omega',
    'approx_order': 1,
}

region_1000 = {
    'name': 'Omega',
    'select': 'all'
}

variable_1 = {
    'name': 'T',
    'kind': 'unknown field',
    'field': 'temperature',
    'order': 0,  # order in the global vector of unknowns
}

variable_2 = {
    'name': 's',
    'kind': 'test field',
    'field': 'temperature',
    'dual': 'T',
}

region_1 = {
    'name': 'Bottom',
    'select': 'vertices in (z < 0.0000001)',
    'kind': 'facet',
}
region_2 = {
    'name': 'Top',
    'select': 'vertices in (z > 0.9999999)',
    'kind': 'facet',
}
region_3 = {
    'name': 'Left',
    'select': 'vertices in (x < 0.0000001)',
    'kind': 'facet',
}

region_4 = {
    'name': 'Right',
    'select': 'vertices in (x > 0.9999999)',
    'kind': 'facet',
}

region_5 = {
    'name': 'Front',
    'select': 'vertices in (y < 0.0000001)',
    'kind': 'facet' 
}

region_6 = {
    'name': 'Back',
    'select': 'vertices in (y < 0.9999999)',
    'kind': 'facet'
}

ebc_1 = {
    'name': 'T_Bottom',
    'region': 'Bottom',
    'dofs': {'T.0': 1.0},
}
ebc_2 = {
    'name': 'T_Top',
    'region': 'Top',
    'dofs': {'T.0': 1.0},
}
ebc_3 = {
    'name': 'T_Left',
    'region': 'Left',
    'dofs': {'T.0': 1.0},
}
ebc_4 = {
    'name': 'T_Right',
    'region': 'Right',
    'dofs': {'T.0': 1.0},
}

ebc_5 = {
    'name': 'T_Front',
    'region': 'Front',
    'dofs': {'T.0': 1.0},
}

ebc_6 = {
    'name': 'T_Back',
    'region': 'Back',
    'dofs': {'T.0': 1.0},
}

integral_1 = {
    'name': 'i',
    'order': 1,
}

equations = {
    'nice_equation': """dw_laplace.i.Omega( coef.val, s, T ) = 0""",
}

solver_0 = {
    'name' : 'ls',
    'kind' : 'ls.scipy_direct',
}

solver_1 = {
    'name': 'newton',
    'kind': 'nls.newton',

    'i_max': 1,
    'eps_a': 1e-10,
    'eps_r': 1.0,
    'macheps': 1e-16,
    'lin_red': 1e-2,  # Linear system error < (eps_a * lin_red).
    'ls_red': 0.1,
    'ls_red_warp': 0.001,
    'ls_on': 1.1,
    'ls_min': 1e-5,
    'check': 0,
    'delta': 1e-6,
}

options = {
    'nls' : 'newton',
    'ls' : 'ls',
}
