r"""
Laplace equation using the long syntax of keywords.

See the tutorial section :ref:`poisson-example-tutorial` for a detailed
explanation. See :ref:`diffusion-poisson_short_syntax` for the short syntax
version.

Find :math:`t` such that:

.. math::
    \int_{\Omega} c \nabla s \cdot \nabla t
    = 0
    \;, \quad \forall s \;.
"""
from __future__ import absolute_import
# from sfepy import data_dir
# filename_mesh = data_dir + '/meshes/3d/cylinder.mesh'
import os
# import pudb

global_back_bound = 0.0
global_front_bound = 1.0

my_dir = os.path.dirname(os.path.realpath(__file__))
del_str = '/scripts/x_shift'
if my_dir.endswith(del_str):
    my_dir = my_dir[:-len(del_str)]


def get_bound(ts, coords, bc=None, problem=None):
    val = coords[:, 1]
    min_val = min(val)
    max_val = max(val)

    # pudb.set_trace()
    print(coords)

    # [t0, t1] = [min_z, max_z]
    [t0, t1] = [global_back_bound, global_front_bound]
    a = (t0 - t1) / (min_val - max_val)
    b = (t1 * min_val - max_val * t0) / (min_val - max_val)

    return a * val + b


functions = {
    'bound_func': (get_bound,),
}

filename_mesh = my_dir + '/meshes/3d/cube_meshpy.vtk'

material_2 = {
    'name': 'coef',
    'values': {'val': 1.0},
}

region_1000 = {
    'name': 'Omega',
    'select': 'all',
}

region_1 = {
    'name': 'Gamma_Top',
    'select': 'vertices in (z > 0.999999)',
    'kind': 'facet',
}

region_2 = {
    'name': 'Gamma_Bot',
    'select': 'vertices in (z < 0.000001)',
    'kind': 'facet',
}

region_3 = {
    'name': 'Gamma_Front',
    'select': 'vertices in (y > 0.999999)',
    'kind': 'facet',
}

region_4 = {
    'name': 'Gamma_Back',
    'select': 'vertices in (y < 0.000001)',
    'kind': 'facet',
}

region_5 = {
    'name': 'Gamma_Left',
    'select': 'vertices in (x < 0.00001)',
    'kind': 'facet',
}

region_6 = {
    'name': 'Gamma_Right',
    'select': 'vertex 1, 2',
    # in (x > 0.999999)',
    # 'kind': 'facet',
    'kind': 'vertex',
}

field_1 = {
    'name': 'temperature',
    'dtype': 'real',
    'shape': (1,),
    'region': 'Omega',
    'approx_order': 1,
}

variable_1 = {
    'name': 't',
    'kind': 'unknown field',
    'field': 'temperature',
    'order': 0,  # order in the global vector of unknowns
}

variable_2 = {
    'name': 's',
    'kind': 'test field',
    'field': 'temperature',
    'dual': 't',
}

ebc_1 = {
    'name': 't1',
    'region': 'Gamma_Top',
    'dofs': {'t.0': 'bound_func'},
}

ebc_2 = {
    'name': 't2',
    'region': 'Gamma_Bot',
    'dofs': {'t.0': 'bound_func'},
}

ebc_3 = {
    'name': 't3',
    'region': 'Gamma_Front',
    'dofs': {'t.0': global_front_bound},
}

ebc_4 = {
    'name': 't4',
    'region': 'Gamma_Back',
    'dofs': {'t.0': global_back_bound},
}

ebc_5 = {
    'name': 't5',
    'region': 'Gamma_Left',
    'dofs': {'t.0': 'bound_func'},
}

ebc_6 = {
    'name': 't6',
    'region': 'Gamma_Right',
    'dofs': {'t.0': 'bound_func'},
}

integral_1 = {
    'name': 'i',
    'order': 2,
}

equations = {
    'Temperature': """dw_laplace.i.Omega( coef.val, s, t ) = 0"""
}

solver_0 = {
    'name': 'ls',
    'kind': 'ls.scipy_direct',
    'method': 'auto',
}

solver_1 = {
    'name': 'newton',
    'kind': 'nls.newton',

    'i_max': 1,
    'eps_a': 1e-12,
    'eps_r': 1.0,
    'macheps': 1e-16,
    'lin_red': 1e-2,  # Linear system error < (eps_a * lin_red).
    'ls_red': 0.1,
    'ls_red_warp': 0.001,
    'ls_on': 1.1,
    'ls_min': 1e-5,
    'check': 0,
    'delta': 1e-6,
}

options = {
    'nls': 'newton',
    'ls': 'ls',
}
