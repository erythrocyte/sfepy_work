INTERPRETATER = python3
SIMULATOR = simple.py
SCRIPTS_DIR = scripts/
MESH_DIR = mesh/
SCRIPTS_FILE = poisson_3d_cube.py


all:
	${INTERPRETATER} ${SIMULATOR} ${SCRIPTS_DIR}${SCRIPTS_FILE} -o x
