# sfepy_work

sfepy fem investigation modules.
on my own meshes

run yourscipts in ${PATH}/move directory as:
python3 ../simple.py ../scripts/filename_in.py 

also you can use options:
$ ./simple.py
Usage: simple.py [options] filename_in
Solve partial differential equations given in a SfePy problem definition file.
Example problem definition files can be found in ``examples/`` directory of the
SfePy top-level directory. This script works with all the examples except those
in ``examples/standalone/``.
Both normal and parametric study runs are supported. A parametric study allows
repeated runs for varying some of the simulation parameters - see
``examples/diffusion/poisson_parametric_study.py`` file.
Options:
--version show program's version number and exit
-h, --help show this help message and exit
-c "key : value, ...", --conf="key : value, ..."
                        override problem description file items, written as
                        python dictionary without surrounding braces
-O "key : value, ...", --options="key : value, ..."
                        override options item of problem description, written
                        as python dictionary without surrounding braces
-d "key : value, ...", --define="key : value, ..."
                        pass given arguments written as python dictionary
                        without surrounding braces to define() function of
                        problem description file
-o filename basename of output file(s) [default: <basename of input file>]
--format=format output file format, one of: {vtk, h5} [default: vtk]
--save-restart=mode if given, save restart files according to the given mode.
--load-restart=filename
if given, load the given restart file
--log=file log all messages to specified file (existing file will be overwritten!)
-q, --quiet do not print any messages to screen
--save-ebc save a zero solution with applied EBCs (Dirichlet boundary conditions)
--save-ebc-nodes save a zero solution with added non-zeros in EBC
                        (Dirichlet boundary conditions) nodes - scalar
                        variables are shown using colors, vector variables
                        using arrows with non-zero components corresponding to
                        constrained components
--save-regions save problem regions as meshes
--save-regions-as-groups
                        save problem regions in a single mesh but mark them by
                        using different element/node group numbers
--save-field-meshes save meshes of problem fields (with extra DOF nodes)
--solve-not do not solve (use in connection with --save-*)
--list=what list data, what can be one of: {terms, solvers}


for more details see: http://sfepy.org/doc/_downloads/sfepy_manual.pdf
